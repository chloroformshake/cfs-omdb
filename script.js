async function loadMovies(searchKey){

    const URL = `http://www.omdbapi.com/?s=${searchKey}&apikey=b3739495`;
    const res = await fetch(`${URL}`);
    const data = await res.json();

    console.log(data);

    if(data.Response != "False") {
        displayMovieCards(data.Search)
    }
}

let cardWrapper = document.querySelector('.card-wrapper')

function displayMovieCards(data) {
    console.log(data);
    cardWrapper.innerHTML = ''

    for (let i = 0; i < data.length; i++) {

        let movieCard = document.createElement('div')
        movieCard.setAttribute('class', 'movie-card')

        let moviePoster = document.createElement('img')
        moviePoster.setAttribute('class', 'movie-poster')
        if (data[i].Poster == "N/A") {
            moviePoster.setAttribute("src", "./images/not-avail.png")
        }
        else {
            moviePoster.setAttribute("src", data[i].Poster)
        }
        moviePoster.setAttribute("alt", data[i].Title)

        
        let movieDetail = document.createElement('div')
        movieDetail.setAttribute('class', 'movie-detail')
        movieDetail.innerHTML = `
            <div class="movie-heading" >
                <div class="movie-title">
                    ${data[i].Title}
                </div>
                <div class="movie-year">
                    (${data[i].Year})
                </div>
            </div>
        `
        movieCard.appendChild(moviePoster)
        movieCard.appendChild(movieDetail)

        cardWrapper.appendChild(movieCard)
    }

    console.log(cardWrapper.children.length);
}

const search = document.querySelector('.search')
search.addEventListener('input', (e) => {

    const messageOne = document.querySelector('.message1')
    const messageTwo = document.querySelector('.message2')
    const messageThree = document.querySelector('.message3')

    let trimmedTerm = (e.target.value).split(/\s+/).join(' ').trim()

    console.log('(' + trimmedTerm + ")");

    if(trimmedTerm.match('^[a-zA-Z0-9_ ]*$')) {


        if(trimmedTerm.length === 0) {
            messageOne.style.display = 'block'
            messageTwo.style.display = 'none'
            messageThree.style.display = 'none'
            cardWrapper.style.display ='none'
        }
        else if(trimmedTerm.length >= 1 && trimmedTerm.length < 3) {
            messageOne.style.display = 'none'
            messageTwo.style.display = 'block'
            messageThree.style.display = 'none'
            cardWrapper.style.display ='none'
        }
        else if(cardWrapper.children.length === 0) {
            messageOne.style.display = 'none'
            messageTwo.style.display = 'none'
            messageThree.style.display = 'block'
            cardWrapper.style.display = 'none'
        }
        else {
            messageOne.style.display = 'none'
            messageTwo.style.display = 'none'
            messageThree.style.display = 'none'
            cardWrapper.style.display = 'grid'
        }
    }
    else {
        messageOne.style.display = 'none'
        messageTwo.style.display = 'none'
        messageThree.style.display = 'block'
        cardWrapper.style.display = 'none'
    }

    for (i = 0; i < cardWrapper.children.length; i++) {

        const titleText = cardWrapper.children[i].children[1].children[0].children[0].textContent.split(/\s+/).join(' ').trim()

        if (titleText.toLowerCase().includes(trimmedTerm.toLowerCase())) {
            cardWrapper.children[i].style.display = 'flex'
        }
        else {
            cardWrapper.children[i].remove()
        }
    }

    loadMovies(trimmedTerm)
})